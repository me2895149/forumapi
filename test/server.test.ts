import { jest } from '@jest/globals'
import { pool } from '../src/db/db'
import server from '../src/server'
import request from 'supertest'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const initializeMockPool = (mockResponse: any) => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}
describe('Testing /users', () => {
	const mockResponse = {
		rows: [
			{ id: '1', username: 'orava', full_name: 'Olli Orava', email: 'orava@gmail.com' },
			{ id: '2', username: 'majava', full_name: 'Mauno Majava', email: 'majava@gmail.com' }
		]
	}
	beforeAll(() => {
		initializeMockPool(mockResponse)
	})
	afterAll(() => {
		jest.clearAllMocks()
	})
	test('Testin GET /users', async () => {
		const response = await request(server).get('/users')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})

	test('Testin GET 1 user from /users by id', async () => {
		const response = await request(server).get('/users/1')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})

describe('Testing /posts', () => {
	const mockResponse = {
		rows: [
			{ id: '1', poster: 'orava', title: 'First post', content: 'Just testing', post_date: '2021-2-13' }
		]
	}
	beforeAll(() => {
		initializeMockPool(mockResponse)
	})
	afterAll(() => {
		jest.clearAllMocks()
	})
	test('Testin GET /posts', async () => {
		const response = await request(server).get('/posts')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe('Testing /comments', () => {
	const mockResponse = {
		rows: [
			{ id: '1', commenting_user: 'orava', commented_post: 'First post', content: 'Awesome post', comment_date: '2021-2-14' },
			{ id: '2', commenting_user: 'majava', commented_post: 'Second post', content: 'Not so awesome post', comment_date: '2021-2-15' }
		]
	}
	beforeAll(() => {
		initializeMockPool(mockResponse)
	})
	afterAll(() => {
		jest.clearAllMocks()
	})
	test('Testin GET on comments from /comments by id', async () => {
		const response = await request(server).get('/comments/1')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})