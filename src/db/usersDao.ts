import { v4 as uuidv4 } from 'uuid'
import  { executeQuery } from './db'
import queries from './queries'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const addUser = async (user: any) => {
	const id = uuidv4()
	const params = [id, ...Object.values(user)]
	console.log(`Inserting a new user ${params[0]}...`)
	const result = await executeQuery(queries.addUser,params)
	console.log(`New user ${id} inserted succesfully.`)
	return result
}

const findUsers = async () => {
	console.log('Requesting for all users...')
	const result = await executeQuery(queries.findUsers)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const findUser = async (id: any) => {
	console.log(`Requesting a user with id:${id}...`)
	const result = await executeQuery(queries.findUser, [id])
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const deleteUser = async (id: any) => {
	console.log(`Deleting a user with id:${id}...`)
	const result = await executeQuery(queries.deleteUser, [id])
	console.log('Deleted user.')
	return result
}

export default { addUser, findUser, findUsers, deleteUser }