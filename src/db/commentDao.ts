import { v4 as uuidv4 } from 'uuid'
import  { executeQuery } from './db'
import queries from './queries'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const addComment = async (comment: any) => {
	const id = uuidv4()
	const params = [id, ...Object.values(comment)]
	console.log(`Inserting a new comment ${params[0]}...`)
	const result = await executeQuery(queries.addComment,params)
	console.log(`New comment ${id} inserted succesfully.`)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const findComment = async (userID: any) => {
	console.log(`Requesting a comments with id:${userID}...`)
	const result = await executeQuery(queries.findComment, [userID])
	console.log(`Found ${result.rows.length} comments.`)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const deleteComment = async (id: any) => {
	console.log(`Deleting a comment with id:${id}...`)
	const result = await executeQuery(queries.deleteComment, [id])
	console.log('Deleted comment.')
	return result
}

export default { findComment, addComment, deleteComment }