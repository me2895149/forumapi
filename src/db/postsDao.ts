import { v4 as uuidv4 } from 'uuid'
import  { executeQuery } from './db'
import queries from './queries'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const addPost = async (post: any) => {
	const id = uuidv4()
	const params = [id, ...Object.values(post)]
	console.log(`Inserting a new post ${params[0]}...`)
	const result = await executeQuery(queries.addPost,params)
	console.log(`New post ${id} inserted succesfully.`)
	return result
}

const findPosts = async () => {
	console.log('Requesting for all posts...')
	const result = await executeQuery(queries.findPosts)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const findPost = async (id: any) => {
	console.log(`Requesting a post with id:${id}...`)
	const result = await executeQuery(queries.findPost, [id])
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const deletePost = async (id: any) => {
	console.log(`Deleting a post with id:${id}...`)
	const result = await executeQuery(queries.deletePosts, [id])
	console.log('Deleted post.')
	return result
}

export default { addPost, findPost, findPosts, deletePost }