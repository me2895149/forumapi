import { Router, Request, Response } from 'express'
import dao from './db/commentDao'

const router = Router()

//Find by id
router.get('/:userID', async (req: Request, res: Response) => {
	const result = await dao.findComment(req.params.id)
	const post = result.rows[0]
	res.status(200).send(post)   
})

//Add new post
router.post('/', async (req: Request, res: Response) => {
	const comment = req.body
	const result = await dao.addComment(comment)
	const storedComments = { id: result.rows[0], ...comment}
	res.status(200).send(storedComments)
})

router.delete('/:id', (req: Request, res: Response) => {
	dao.deleteComment(req.params.id)
	res.status(200).send('Deleted comment')
})

export default router