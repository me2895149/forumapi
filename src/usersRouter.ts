import { Router, Request, Response } from 'express'
import dao from './db/usersDao'

const router = Router()


//Findall
router.get('/', async (req: Request, res: Response) => {
	const result = await dao.findUsers()
	res.status(200).send(result.rows)   
})

//Find by id
router.get('/:id', async (req: Request, res: Response) => {
	const result = await dao.findUser(req.params.id)
	const user = result.rows[0]
	res.status(200).send(user)   
})

//Add new user
router.post('/', async (req: Request, res: Response) => {
	const user = req.body
	const result = await dao.addUser(user)
	const storedUsers = { id: result.rows[0], ...user}
	res.status(200).send(storedUsers)
})

router.delete('/:id', (req: Request, res: Response) => {
	dao.deleteUser(req.params.id)
	res.status(200).send('Deleted user')
})

export default router