//Users queries
const findUsers = 'SELECT id, username FROM users;'
const findUser = 'SELECT * FROM users WHERE id = $1'
const addUser = 'INSERT INTO users (id, username, full_name, email) VALUES ($1, $2, $3, $4);'
const deleteUser = 'DELETE FROM users WHERE id = $1;'

//Posts queries
const findPosts = 'SELECT posts.id, posts.title, users.id from posts JOIN users ON posts.poster = users.username;'
const findPost = 'SELECT * FROM posts JOIN comments ON posts.title = comments.commented_post WHERE posts.id = $1;'
const addPost = 'INSERT INTO posts (id, poster, title, content, post_date) VALUES ($1, $2, $3, $4, $5);'
const deletePosts = 'DELETE FROM posts WHERE id = $1;'

//Comments queries
const addComment = 'INSERT INTO comments (id, commenting_user, commented_post, content, comment_date) VALUES ($1, $2, $3, $4, $5);'
const findComment = 'SELECT * FROM comments JOIN users ON comments.commenting_user = users.username WHERE users.id = $1;'
const deleteComment = 'DELETE FROM comments WHERE id = $1;'

export default { findUser, findUsers, addUser, addPost, findPost, findPosts, addComment, findComment, deleteUser, deletePosts, deleteComment }
