import server from './server'

const { PORT } = process.env
server.listen(PORT, () => {
	console.log('Forum API listening to port', PORT)
})
