import { Router, Request, Response } from 'express'
import dao from './db/postsDao'

const router = Router()


//Findall
router.get('/', async (req: Request, res: Response) => {
	const result = await dao.findPosts()
	res.status(200).send(result.rows)   
})

//Find by id
router.get('/:id', async (req: Request, res: Response) => {
	const result = await dao.findPost(req.params.id)
	const post = result.rows[0]
	res.status(200).send(post)   
})

//Add new post
router.post('/', async (req: Request, res: Response) => {
	const post = req.body
	const result = await dao.addPost(post)
	const storedPosts = { id: result.rows[0], ...post}
	res.status(200).send(storedPosts)
})

router.delete('/:id', (req: Request, res: Response) => {
	dao.deletePost(req.params.id)
	res.status(200).send('Deleted post')
})

export default router